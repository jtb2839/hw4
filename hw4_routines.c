#include "hw4_routines.h"
#include "stdlib.h"
#include "math.h"
double integrate(double start, double end, int iterations)
{
	double length=end-start;
	double dl = length/iterations;
	double sum=4/(1+pow(start,2))+4/(1+pow(end,2));
	double i;
	int go=1;
	for(i=start+dl;i<end-dl;i=i+dl)
	{
		if(go%2==0)
		{
			sum=sum+2*4/(1+pow(i,2));
		}
		else
		{
			sum=sum+4*4/(1+pow(i,2));
		}
		go=go+1;
	}
	sum=sum*dl/3;
	return sum;
}

#include "stdlib.h"
#include "math.h"
#include "hw4_routines.h"
#include <stdio.h>
#include <mpi.h>

int main(int argc, char *argv[])
{
	int iterations=100000;
	int currproc, totalprocs;
	double pi_guess, sum;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &currproc);
	MPI_Comm_size(MPI_COMM_WORLD, &totalprocs);
	sum=integrate(currproc*1.0/totalprocs,(currproc+1)*1.0/totalprocs,iterations);
	MPI_Reduce(&sum,&pi_guess,1,MPI_DOUBLE,MPI_SUM,0,MPI_COMM_WORLD);
	if(currproc==0)
	{
		printf("Pi estimate with %d processors is %1.15f\n",totalprocs,pi_guess);
		printf("Used Simpson's Method, %d iterations\n\n",iterations*totalprocs);
		if(totalprocs==8)
		{
			printf("I believe that the reason why the answer is getting less accurate with more cores is that the method is inaccurate at evaluating pi after a certain number of iterations.");
		}
	}
}

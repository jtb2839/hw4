CC:= mpicc 

hw4_mpi_pi: hw4_mpi_pi.o hw4_routines.o
	$(CC) -o $@ $^ -lm
hw4_mpi_pi.o: hw4_mpi_pi.c hw4_routines.h
	$(CC) -c $<
hw4_routines.o: hw4_routines.c hw4_routines.h
	$(CC) -c $<
run: hw4_mpi_pi
	./hw4_mpi_pi
